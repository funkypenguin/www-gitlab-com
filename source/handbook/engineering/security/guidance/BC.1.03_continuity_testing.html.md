---
layout: markdown_page
title: "BC.1.03 - Continuity Testing Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# BC.1.03 - Continuity Testing

## Control Statement

GitLab performs business contingency and disaster recovery tests quarterly and ensures the following:

* Tests are executed with relevant contingency teams.
* Test results are documented.
* Corrective actions are taken for exceptions noted.
* Plans are updated based on results.

## Context

The business continuity plan is only useful if it is both maintained and validated. The testing part of this process is meant to be that validation and determines the efficacy of the plan. The purpose of this control is to determine if the business continuity plan would work in the event of a disruption to normal GitLab operations.  The BC plan these three main categories:

* Recovery Planning:  Ensuring that Recovery processes and procedures are executed and maintained to timely restoration of systems or assets affected by any disruptive event.
* Improvements:  Recovery planning and processes are improved by incorporating lessons learned into future activities.
* Communications:  Restoration activities are coordinated with internal and external parties, such as coordinating centers, Internet Service Providers, system owners, victims and vendors.

## Scope

All parts of the business continuity plan should be tested. All teams and services that have a documented business continuity plan should have a corresponding documented test.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.03_continuity_testing.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.03_continuity_testing.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/BC.1.03_continuity_testing.md).

## Framework Mapping

* ISO
  * A.17.1.2
  * A.17.1.3
* SOC2 CC
  * CC7.5
  * CC9.1
* SOC2 Availability
  * A1.3
