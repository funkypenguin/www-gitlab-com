---
layout: markdown_page
title: "Throughput"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

[Throughput](https://weblogs.asp.net/wallen/throughput-vs-velocity), is a
measure of the total number of MRs that are completed and in production in a
given period of time.  Unlike velocity, throughput does not require the use of
story points or weights, instead we measure the number of MRs completed by a
team in the span of a week or a release.  Each MR is represented by 1
unit/point.  This calculation happens after the time period is complete and no
pre-planning is required to capture this metric.  The total count should not be
limited to only MRs that deliver features, it's important to include
[engineering proposed](/handbook/engineering/#engineering-proposed-initiatives)
MRs in this count as well.  This will ensure that we properly reflect the team's
capacity in a consistent way and focus on delivering at a predictable rate.

## Implementation

Each merge request must have one of the following labels. These labels
assign a category in the charts. If an MR has more than one of these
labels, the highest one in the list takes precedence.

- ~"Community contribution"
- ~security
- ~bug
- ~feature
- ~backstage

If it does not have any of these, it will be tracked in the 'undefined'
bucket instead. The Engineering Manager for each team is ultimately
responsible for ensuring that these labels are set correctly, and should
do this as a manual process on a schedule that is appropriate for their
time.

Throughput charts are available on the [quality dashboard] for each team.

[quality dashboard]: http://quality-dashboard.gitlap.com

## Why we adopted this model

- The goal for using this measure is to incentivize teams to break MRs to the smallest deliverable which lead to a smaller set of changes and the many benefits that come along with that.
- This practice aligns with one of our core values: [Iteration](/handbook/values/#iteration), do the smallest thing possible and get it out as quickly as possible.
- Instead of spending time sizing and figuring out the weight of an issue, we should put this effort toward breaking issues to the smallest [deliverable](/handbook/engineering/#code-quality-and-standards).
- Since throughput is a measure of actual work completed, it is far more
accurate than using weights.
- Throughput is a simpler model to implement for new teams since the measure
  is the count of small well defined MRs.
- Unlike weights which may be estimated differently from one team to another,
  throughput can be normalized across every engineering team.

## A few notes to consider when using this model

- There are many activities such as code reviews, meetings, planning that we do not count as units of work independently, they are however accounted for as part of the delivery of an issue whether it be feature work or technical debt.  The team's rate of delivering code to production is what we are trying to measure.
- This metric is a tool for an Engineering Manager to [determine the capacity
  of the team](/handbook/engineering/management/#project-management) for each given release.
  You need to gather a few milestones worth of data to get a sense of your week to week delivery rate.
- While there is no scoring required with this model, there is still value
  for an Engineering Manager to look through each issue they are committing to
  in any given release and making sure they are well defined small deliverables.

## How to read and interpret the data
- Review the chart weekly and take notes of the trend week to week.
Is your delivery rate staying consistent? If not,
take note of anything that might be impacting the team's capacity such as holidays. 
- If the team is merging most of their MRs during a particular week of a milestone, it is 
good to discuss this further with the team and understand what impediments might be leading to this
trend. 
- Take note of your categories which are based on the labels listed above. 
Are you delivering in mostly one or two categories over all others? You may need to discuss
this further with your Product Manager. 
- Take note of your team's focus on community contribution as an example. If the team is able
to consistently merge MRs in this categories, celebrate it. 
- If you see `undefined` MRs represented, spend some time to review your team's MRs
 and add labels so you can get a more accurate reflection of your investment. It could take up to
 a day for these updates to show up on the quality dashboard. Also good to keep in mind that 
 the data here represents contributions in multiple projects. Label hygiene is not enforced across
 all of them.

When combined with cycle time, throughput is a great metric to help you identify areas of improvement and possible bottlenecks that the team can work to address.

