---
layout: markdown_page
title: "GitLab Internal Acceptable Use Policy"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Purpose

This policy specifies requirements related to the use of GitLab computing resources and data assets by GitLab team members so as to protect our customers, team members, contractors, company, and other partners from harm caused by both deliberate and inadvertent misuse. Our intention in publishing this policy is not to impose restrictions but outline information security guidelines intended to protect GitLab assets.

It is the responsibility of every member of our Community to interact with GitLab computing resources and data in a secure manner and to that end we provide the following acceptable use standards related to computing resources, company and customer data, mobile and tablet devices, and removable and external media storage devices.

## Scope

This policy applies to all GitLab team members, contractors, advisors, and contracted parties interacting with GitLab computing resources and accessing company and customer data.

## Acceptable Use and Security Requirements of Computing Resources at GitLab

### General Use and Ownership

GitLab-managed assets are provided to conduct GitLab business with consideration given for limited personal use.

Those receiving GitLab-provided assets are responsible for exercising good judgment when using GitLab-managed computers and accessing GitLab-managed data.

As per the [onboarding issue procedures](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) outlined in our handbook, evidence of device encryption and device serial number must be provided to PeopleOps prior to the completion of onboarding period.

### Security and Proprietary Information

All GitLab data is categorized and must be handled in accordance with the [Data Classification Policy](https://about.gitlab.com/handbook/engineering/security/data-classification-policy.html). All computing assets that connect to any part of the GitLab network, or 3rd party services that are used by GitLab, must comply with the applicable standards.

### Unacceptable Use

Team members and contractors may under no circumstances use GitLab-managed resources for activities that are illegal or prohibited under applicable law.

### Unacceptable System and Network Activities

Prohibited system and network activities include, but are not limited to, the following:  

- Violations of the rights of any person or company protected by copyright, trade secret, patent or other intellectual property, or similar laws or regulations.
- Unauthorized copying, distribution, or use of copyrighted material.
- Exporting software, technical information, encryption software, or technology in violation of international or national export control laws.
- Intentional introduction of malicious programs into GitLab networks or any GitLab-managed computing device.
- Intentional misuse of any GitLab-managed computing device or GitLab networks (e.g. for cryptocurrency mining, botnet control etc.).
- Sharing your credentials for any GitLab-managed computer or 3rd party service that GitLab uses, with others, or allowing use of your account or a GitLab-managed computer by others. This prohibition does not apply to single-sign-on or similar technologies, the use of which is approved.
- Using a GitLab computing asset to engage in procuring or transmitting material that is in violation of sexual harassment policies or that creates a hostile workplace.
- Making fraudulent offers of products, items, or services originating from any GitLab account.  
- Intentionally accessing data or logging into a computer or account that the team member or contractor is not authorized to access, or disrupting network communication, computer processing, or access.
- Executing any form of network monitoring that intercepts data not intended for the team member’s or contractor's computer, unless when troubleshooting networking issues for the benefit of GitLab.
- Circumventing user authentication or security of any computer host, network, or account used by GitLab.
- Tunneling between network segments or security zones (e.g., `gprd`, `gstg`, `ops`,`ci`), unless when troubleshooting issues for the benefit of GitLab.

### Unacceptable Email and Communications Activities

Forwarding of company-confidential business emails, and documents to personal external email addresses.

> Note: GitLab may retrieve messages from archives and servers without prior notice if GitLab has sufficient reason to do so. If deemed necessary, this investigation will be conducted with the knowledge and approval of Security, People Ops, and Legal Departments.

### Return of GitLab-Owned Assets

All GitLab-owned computing resources must be [returned](https://about.gitlab.com/handbook/offboarding/#returning-property) upon separation from the company.

### Personal Mobile Phone and Tablet Usage

All personal mobile computing devices used to access GitLab-managed data, including but not limited to email and GitLab.com, must be passcode-enabled. 2FA will be enforced by the Security team for all employee and contractor GitLab.com and GSuite accounts. Mobile computing best practices dictate that these devices should be running the latest version of the operating system and all new patches applied. For assistance with determining the suitability of your mobile device, please contact the Security Team.

### Mobile Messaging
All GitLab-related conversations need to take place in Slack. It is strongly recommended that the official Slack applications, or Slack web application, are used for mobile messaging. Downloads are available for [iOS](https://itunes.apple.com/app/slack-app/id618783545?ls=1&mt=8) and [Android](https://play.google.com/store/apps/details?id=com.Slack). Whereas it may be more convenient to use an integrated chat application that puts all of your conversations in one place, the use of these applications can unintentionally lead to work-related conversations crossing platforms, or being sent to external contacts. The use of Slack for all work communications assists with our security and compliance efforts. For example, in the case of an incident response issue, it may be necessary to review a conversation to understand the order in which events occurred, or to provide evidence that the chain of custody has been maintained for forensic evidence during a handoff. 

For [video calls](https://about.gitlab.com/handbook/communication/#video-calls), and as a back-up to Slack, we prefer Zoom. Zoom chats are an acceptable alternative to Slack when in a video call. If the conversation is interesting to others or may be needed for a retrospective, consider recording the call. 

### Use of External Media on Company Assets

The use of removable and external storage devices such as USB flash drives and external backup drives on company-managed devices is not officially sanctioned. If there is a business need for the use of an external storage device, such as a flash drive or an external hard drive on company devices, please contact the Security Team to determine the most suitable encryption-enabled device. All external and removable storage devices must be encrypted and protected by a passcode.

### Lost or Stolen Procedures

GitLab provides a panic@gitlab.com email address and a [lost or stolen procedure](https://about.gitlab.com/handbook/security/#panic-email) for team members to use in situations that require an immediate security response. Should a team member lose a device such as a thumb drive, Yubikey, mobile phone, tablet, laptop, etc. that contains their credentials or other GitLab-sensitive data, they should send an email to panic@gitlab.com right away. When the production and security teams receive an email sent to this address it will be handled immediately. Using this address provides an excellent way to limit the damage caused by a loss of one of these devices.

### Policy Compliance

Compliance with this policy will be verified through various methods, including but not limited to, automated reporting, audits, and feedback to the policy owner.

Any team member or contractor found to be in violation of this policy may be subject to disciplinary action, up to and including termination of employment, or contractual agreement.

Exceptions to this policy must be approved by Security, Legal and PeopleOps Departments.

### Consultations

To consult with the Security Team, use the appropriate contact: security@gitlab.com, or create an issue in the [Security Compliance tracker](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues).

### Related Documents and Handbook Entries

[Onboarding Issue](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)

[Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY)

[Asset return procedure](https://about.gitlab.com/handbook/offboarding/#returning-property)

[Lost or stolen asset procedure](https://about.gitlab.com/handbook/security/#panic-email)
