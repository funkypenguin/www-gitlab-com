---
layout: markdown_page
title: "Past GitLab Hackathon Events"
---

## Q4'2018 Hackathon (November 14-15)
* [Event announcement blog post](https://about.gitlab.com/2018/10/23/q4-hackathon-announcement/)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/9)
* [Event recap blog post](https://about.gitlab.com/2018/11/29/q4-hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| GitLab community | David Planella/Ray Paik (@dplanella/@rpaik) | [Slides](https://docs.google.com/presentation/d/176wP9znxdz_egFx3wh7w9fSCd1oOGjLAfrnzrt1yRBs/edit#slide=id.g3db14447a7_0_0)/[Video](https://www.youtube.com/watch?v=BujIJE5N0Rc) |
| Meltano | Jacob Schatz (@jschatz1) | [Video](https://www.youtube.com/watch?v=0WatxtwgLDE) |
| Omnibus | DJ Mountney (@twk3) | [Video](https://www.youtube.com/watch?v=mCec7g6Ml70) |
| GitLab Application Architecture | Stan Hu (@stanhu) | [Slides](https://docs.google.com/presentation/d/1qigstMdjhFmulRGo3f-NeFOBN_JXsF_3a8zJblo11zY)/[Video](https://www.youtube.com/watch?v=0GVtrxZ5_a8) |
| Gitter | Eric Eastwood (@MadLittleMods) | [Video](https://www.youtube.com/watch?v=LFWTW6PbJOQ) |
| Event wrap-up | Ray Paik (@rpaik) | [Slides](https://docs.google.com/presentation/d/11nsZ_4pJDEVSfTUKIFOZYH1mg7QVw0qOO71JmyrWPIU/edit#slide=id.g3db14447a7_0_0)/[Video](https://www.youtube.com/watch?v=jvDKdrHRoZ0) |

* [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/10) 

## Q3'2018 Hackathon (September 27-28)

* [Event announcement blog post](/2018/09/17/gitlab-hackathon/)
* [Hackathon prize winners](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/1)
* [Event recap blog post](/2018/10/09/hackathon-recap/)
* Materials/recordings from tutorial sessions

| Topic | Speaker(s) | Materials |
| ------| -----------| --------- |
| Event kickoff | David Planella(@dplanella)/Ray Paik(@rpaik) | [Slides](https://docs.google.com/presentation/d/1udT8v45w6LoaIGZmqqMN0v0dn1mqcMkZ1PPcfdwiA9k/edit#slide=id.g3db14447a7_0_0)/[Video](https://www.youtube.com/watch?v=v1QNNpZ79mA) |
| GitLab Development Kit (GDK) | Toon Claes (@toon) | [Video](https://www.youtube.com/watch?v=gxn-0KSfNaU) |
| Documentation | Mike Lewis(@mikelewis)/ Achilleas Pipinellis(@axil) | [Slides](https://docs.google.com/presentation/d/1ZpjBPS1gG0FKPgX7zxfgRjK_1YzApdw1M1CedsRrDfk/)/[Video](https://youtu.be/8GT2XOkpSi4) |
| Internationalization/Translation | Hannes Rosenögger(@haynes) | [Video](https://youtu.be/LJ9oSSx0qyY) |
| Day 1 wrap-up | Ray Paik(@rpaik) | [Slides](https://docs.google.com/presentation/d/1jtIs00GOGaweozR_rzJ0XR2QePxi_1GXA3Kl-cvTchk/edit#slide=id.g42dc46e089_0_9)/[Video](https://youtu.be/tONnxJ0_yEM)|
| UX design workflow | Sarrah Vesselov (@sarrahvesselov) | [Slides](https://docs.google.com/presentation/d/1wKjRc7tXeinjwfwFZstjLzIrea1gLc2VJIUagApCnBI)/[Video](https://youtu.be/q_nq5OCiktE) | 
| Merge Request Coach | Clement Ho (@ClemMakesApps) | [Video](https://youtu.be/daCFv9tAQXw)|
| Event wrap-up | Ray Paik(@rpaik) | [Slides](https://docs.google.com/presentation/d/18niYSpi7aTZ_Biczn3mPxo8_qyjvYVn_rZW4SjHk6OA/edit#slide=id.g3db14447a7_0_0)/[Video](https://youtu.be/rBwKfBVi4Qw) | 

* [Community MR's during the Hackathon](https://gitlab.com/gitlab-com/marketing/community-relations/contributor-program/hackathon/issues/4)