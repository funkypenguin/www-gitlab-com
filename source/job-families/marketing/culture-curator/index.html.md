---
layout: job_family_page
title: "Culture Curator"
---

## Culture Curator

The GitLab Culture Curator is a unique position on the corporate marketing team to develop and tell the story of GitLab’s remote culture. As a pioneer of remote work, this position will tell the story of GitLab’s global remote employees, remote work processes, transparent culture and the movement to remote work that GitLab has created. 

### Responsibilities

- Own the story around GitLab’s remote culture and transparent work philosophy that is changing the future of how people work.
- Create and execute a content strategy and thought leadership platform to tell GitLab’s story about remote work and transparent culture.
- Use such tactics as online publishing, social, events, public relations, and partnerships to demonstrate GitLab’s leadership in the future of work movement. 
- Create and execute a standalone (digital publication) focused on GitLab’s leadership in remote work culture in the context of the broader movement. Manage content production from start to finish.
- Work with employees around the globe to highlight remote culture stories
- Collaborate directly with the CEO to document GitLab’s remote and transparent culture philosophies.
- Follow the movement to remote work with a journalist’s eye, and cover as appropriate.
- Plug in to bigger opportunities to drive awareness around GitLab’s remote work culture. 
- Employ an ethnographic storytelling approach to document and share authentic, credible stories from the movement offering insights that can be applied to solve problems throughout the organization and also adopted by others outside of GitLab.
- Work cross-functionally to develop and share key insights that can be applied across teams and programs: HR (Culture & Recruiting), Product Management and Design (Ethnographic research), Community, Internal Communications, etc. 

### Requirements

- 5 years experience in content marketing, journalism or communications. 
- Proven track record developing content marketing strategies and thought leadership campaigns.
- Ability to travel frequently, internationally 
- A natural storyteller with excellent narration and writing skills.
- Experience leading the development of exciting and effective awareness and brand marketing campaigns.
- Able to coordinate across many teams and perform in fast-moving startup environment.
- Proven ability to be self-directed and work with minimal supervision.
- Outstanding written and verbal communications skills.
- You share our values, and work in accordance with those values.
