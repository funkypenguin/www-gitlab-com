---
layout: job_family_page
title: "Technical Accounting Manager"
---

GitLab is looking for an essential member to further stregthen out finance department. You will join our team in its early stages and support in developing a highly efficient, world class accounting process. We expect you to deeply understand GAAP accounting principles, and how to apply those principles to GitLab’s financial statements.


## Responsibilities

* Assist in determining and documenting the Company’s accounting policies and position on relevant transactions.
* Review of non-recurring journal entries to determine appropriate classification.
* Must have good technical research skills.
* Must be able to work collaboratively with the operational accounting team and business functions.
* Provide periodic technical accounting trainings to the members of the Accounting organization.
* Coordinate with external audit (Ernst & Young) on technical accounting issues.
* This position will initially be an individual contributor but we expect this person to build a highly functioning, distributed team over time.
* Provide support to the accounting close process, including preparation of US GAAP financial statements.

## Requirements

* Proven work experience in a Technical Accounting role.
* Fair knowledge of SEC filing requirements, experience highly preferred.
* Must have at least three years of work experience in a Big 4 CPA firm.
* Must have knowledge of US GAAP.
* Strong working knowledge of GAAP principles and financial statements.
* Proficient with google sheets.
* Revenue recognition, namely 606, experience highly preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Technical Accounting Manager
- Next, candidates will be invited to schedule a 45 minute interview with our Accounting and External Reporting Manager
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
