---
layout: markdown_page
title: "Building applications that meet HIPAA standards"
---
## See how using GitLab can help you with HIPAA compliance

The Health Insurance Portability and Accountability Act (HIPAA) sets the standard for sensitive patient data protection.

Within HIPAA regulations, "The Privacy Rule, or Standards for Privacy of Individually Identifiable Health Information, establishes national standards for the protection of certain health information. The Security Standards for the Protection of Electronic Protected Health Information (the Security Rule) establish a national set of security standards for protecting certain health information that is held or transferred in electronic form. The Security Rule operationalizes the protections contained in the Privacy Rule." See the [HHS website](https://www.hhs.gov/hipaa/for-professionals/security/laws-regulations/index.html) for more.

The Security Rule requires companies that deal with Protected Health Information (PHI) must have physical, network, and process security measures in place and follow them to ensure HIPAA Compliance.


### [Security Rules](https://msdn.microsoft.com/en-us/library/aa480484.aspx#regcompliance_demystified_topic3)

1. Administrative Safeguards must establish and enforce company privacy policies and procedures (for example, disaster recovery and contingency plans).
1. Physical Safeguards must encompass restrictions and rules that deal with physical access to facilities and machines, access controls, as well as the associated policies and procedures that deal with the physical entities in the organization.
1. Technical Standards contain all of the safeguards and practices that relate to the intangible information contained in the organizations computer systems such as intrusion prevention, data corroboration, and access controls. Here we will focus on the technical standards section because they contain most of the actionable items for developers.

### [Principles](https://msdn.microsoft.com/en-us/library/aa480484.aspx#regcompliance_demystified_topic3)

The security provisions are intended to:
* Ensure confidentiality, integrity, and availability of all ePHI that the health care entity creates, receives, transmits, or maintains.  
  * Confidentiality - Design software so that the encryption algorithms are replaceable and key sizes can be easily increased so that encryption strength can keep pace with advances in computing power and cracking algorithms.  
  * Integrity - Records should not be modifiable by unauthorized people or entities.  
  * Availability - Event logs should contain enough information to make it possible to reconstruct system activity up to the point of failure so that the error can be quickly resolved and fixed.  
* Prevent disclosure of any ePHI information that is not permitted or required.  
* Ensure that system information is available for auditing trails. Any actions that might need to be traced must be documented.   
* Ensure that authentication is in place so that specific workers or entities are who they say they are. It is necessary to know that the entity or person that is working with the data is legitimate.

Within the technical standard, there are several controls required. The controls that can be affected by the software development lifecycle are shown, along with GitLab features that contribute to their compliance.


<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-xldj{border-color:inherit;text-align:left}
</style>
<table class="tg" style="undefined;table-layout: fixed; width: 100%">
<colgroup>
<col style="width: 90px">
<col style="width: 200px">
<col style="width: 200px">
<col style="width: 200px">
</colgroup>
  <tr>
    <th class="tg-xldj">Standard</th>
    <th class="tg-xldj">Requirement</th>
    <th class="tg-xldj">Applicable Specifications</th>
    <th class="tg-xldj">How GitLab helps</th>
  </tr>
  <tr>
    <td class="tg-xldj">Access Controls</td>
    <td class="tg-xldj">Access controls should enable authorized users to access the minimum necessary information needed to perform job functions.</td>
    <td class="tg-xldj">Unique User Identification allows an entity to track specific user activity when that user is logged into an information system and hold users accountable for functions performed on information systems with EPHI when logged into those systems.</td>
    <td class="tg-xldj">GitLab helps users comply with Unique User Identification via: <br><a href="https://docs.gitlab.com/ee/user/project/protected_branches.html">1. Protected branches</a><br>
<a href="https://docs.gitlab.com/ee/user/project/merge_requests/merge_request_approvals.html">2. Only authorized people can approve the code via Merge request approvals</a><br>
<a href="https://docs.gitlab.com/ee/api/protected_branches.html#protect-repository-branches">3. Unprotect permission</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ce/issues/44041">4. Approval jobs in CI pipelines</a><br>
<a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/7176">5. Two-person access controls</a><br>
<a href="https://about.gitlab.com/2018/04/20/gitlab-tiers/#gitlab-self-hosted">6. It is Self-managed</a> so you can install in a <a href="https://aws.amazon.com/vpc/">Virtual Private Cloud</a> and have app and data under your control.<br>
<a
href="https://docs.gitlab.com/ee/user/permissions.html">7. Permissions</a>
</td>
  </tr>
  <tr>
  <td class="tg-xldj">Audit Controls</td>
  <td class="tg-xldj">Entities must “Implement hardware, software, and/or procedural mechanisms that record and examine activity in information systems that contain or use electronic protected health information.”</td>
  <td class="tg-xldj">Some level of audit controls with a reporting method, such as audit reports. These controls are useful for recording and examining information system activity, especially when determining if a security violation occurred.<br>
  </td>
  <td class="tg-xldj">1. One concept of a user across the lifecycle to ensure the right level of permissions and access<br>
  <a
  href="https://docs.gitlab.com/ee/administration/logs.html">2. Audit logs</a><br>
  <a
  href="https://docs.gitlab.com/ee/administration/audit_events.html">3. Audit events</a><br>
  <a
  href="https://docs.gitlab.com/omnibus/docker/README.html#where-is-the-data-stored">4. Container image retention</a><br>
  <a
  href="https://docs.gitlab.com/ee/administration/job_artifacts.html#storing-job-artifacts">5. Artifact retention</a><br>
  <a
  href="https://docs.gitlab.com/ee/development/testing_guide/end_to_end_tests.html#testing-code-in-merge-requests">6. Test result retention</a><br>
  <a
  href="https://docs.gitlab.com/ee/administration/audit_events.html#audit-events">7. Audit event</a> helps you view changes made.<br>
  8. Future: Disable squash of commits<br>
  9. Future: Prevent purge<br></td>
</tr>
<tr>
  <td class="tg-xldj">Integrity Controls</td>
  <td class="tg-xldj"> Implement policies and procedures to protect electronic protected health information from being altered or destroyed in an unauthorized manner.<br>
  Note: Application Security Testing can help identify vulnerabilities that enable unauthorized access to data, logic, and reporting. </td>
  <td class="tg-xldj">Once covered entities have identified risks to the integrity of their data, they must identify security measures that will reduce the risks. Best practices include:<br>* Scan applications regularly for vulnerabilities.<br>* Establish criteria for the prioritization of vulnerabilities and remediation activities.<br>* Pay special attention to internally or custom developed applications with dynamic and static analysis.<br>* Establish secure coding as a culture, and provide qualified training on secure coding.<br>* Establish and document a secure development life-cycle approach that fits your business and developers.<br></td>
  <td class="tg-xldj">
  <a href="https://docs.gitlab.com/ee/user/project/merge_requests/sast.html">1. SAST</a><br>
  <a href="https://docs.gitlab.com/ee/user/project/merge_requests/dast.html">2. DAST</a><br>
  <a href="https://docs.gitlab.com/ee/user/project/merge_requests/dependency_scanning.html">3. Dependency Scanning</a><br>
  <a href="https://docs.gitlab.com/ee/user/project/merge_requests/container_scanning.html">4. Container Scanning</a><br>
  <a href="https://about.gitlab.com/2018/07/22/gitlab-11-1-released/#security-dashboard-for-projects">5. Security Dashboard</a><br>
  <a
  href="https://about.gitlab.com/handbook/product/#security-paradigm">6. Security Paradigm</a><br>
  In addition to Application Security Testing to help you deliver secure apps, GitLab's own application has security to prevent unauthorized access to the application code as well as audit and logging capabilities of changes to the code.</td>
  </tr>
  <tr>

To learn how GitLab's security scanning features may be able to help with HIPAA compliance, read our <a href="https://about.gitlab.com/2019/04/10/streamlining-hipaa-risk-analysis-requirements-with-gitlab/">blog post</a> on streamlining HIPAA risk analysis requirements with GitLab.