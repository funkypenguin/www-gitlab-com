---
layout: markdown_page
title: "Category Vision - Code Quality"
---

- TOC
{:toc}

## Code Quality

Automatically analyze your source code to surface issues and see if quality is improving or getting worse with the latest commit.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=code%20quality&sort=milestone)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

Up next for code quality is [gitlab-ce#14974](https://gitlab.com/gitlab-org/gitlab-ce/issues/14974), which will allow teams to set a threshold of code quality degradation beyond which the pipeline will fail to force a decision on introducing these types of issues or not. It will help bring the importance of code quality to the fore and help teams deliver faster.

## Competitive Landscape

### Azure DevOps

Azure DevOps does not offer in-product quality testing in the same way we do with CodeClimate, but does have a number of easy to find and install plugins in their [marketplace](https://marketplace.visualstudio.com/search?term=code%20quality&target=AzureDevOps&category=All%20categories&sortBy=Relevance) that are both paid and free. Their [SonarQube plugin](https://marketplace.visualstudio.com/items?itemName=SonarSource.sonarqube) appears to be the most popular, though it seems to have some challenges with the rating.

In order to remain ahead of Azure DevOps, we should continue to push forward the feature capability of our own open-source integration with CodeClimate. Issues like [gitlab-ee#2766](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) (per-branch view for how code quality progresses over time) moves both our vision forward as well as ensures we have a high quality integration in our product.

## Analyst Landscape

We do not engage with analysts for code quality market analysis.

## Top Customer Success/Sales Issue(s)

An interesting suggestion from the CS team is [gitlab-ee#8406](https://gitlab.com/gitlab-org/gitlab-ee/issues/8406), which is beyond just code quality but code quality could lead the way. This item introduces instance wide code statistics, showing (for example) programming languages used, code quality statistics, and code coverage.

## Top Customer Issue(s)

The most popular item (with 6 upvotes) is [gitlab-ee#4189](https://gitlab.com/gitlab-org/gitlab-ee/issues/4189), which creates a CI view for code quality. Currently the code quality information is shown in the MR widget.

## Top Internal Customer Issue(s)

None yet.

## Top Vision Item(s)

In terms of moving the vision forward, [gitlab-ee#2766](https://gitlab.com/gitlab-org/gitlab-ee/issues/2766) (which creates a per-branch view for code quality) is an interesting approach. This brings code quality tracking out of the MR widget, and looks at how code quality is changing over a longer period of time in the context of a branch (or master.)